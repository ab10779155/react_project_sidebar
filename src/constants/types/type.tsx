export type TTabData =
  {
    title: string;
    path: string;
    className: string;
    activeClassName: string;
    content: string;
    isAdmin: boolean;
  }
