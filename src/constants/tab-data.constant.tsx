import { TTabData } from "./types";

export const TabData: TTabData[] = [
    {
      title: "News",
      path: "/main/home/news",
      className: "tabs",
      activeClassName: "active-tabs",
      content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati 
      praesentium incidunt quia aspernatur quasi quidem facilis quo nihil
        vel voluptatum?`,
      isAdmin: false  
    },
    {
      title: "Science",
      path: "/main/home/science",
      className: "tabs",
      activeClassName: "active-tabs",
      content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente
      voluptatum qui adipisci.`,
      isAdmin: false 
    },
    {
      title: "World",
      path: "/main/home/world",
      className: "tabs",
      activeClassName: "active-tabs",
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos sed
      nostrum rerum laudantium totam unde adipisci incidunt modi alias!
      Accusamus in quia odit aspernatur provident et ad vel distinctio
      recusandae totam quidem repudiandae omnis veritatis nostrum laboriosam
      architecto optio rem, dignissimos voluptatum beatae aperiam voluptatem
      atque. Beatae rerum dolores sunt.`,
      isAdmin: false 
    },
    {
      title: "Personal",
      path: "/main/home/personal",
      className: "tabs",
      activeClassName: "active-tabs",
      content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos sed
      nostrum rerum laudantium totam unde adipisci incidunt modi alias!
      Accusamus in quia odit aspernatur provident et ad vel distinctio
      recusandae totam quidem repudiandae omnis veritatis nostrum laboriosam
      architecto optio rem, dignissimos voluptatum beatae aperiam voluptatem
      atque. Beatae rerum dolores sunt. Lorem ipsum dolor sit amet,
      consectetur adipisicing elit. Eos sed nostrum rerum laudantium totam
      unde adipisci incidunt modi alias! Accusamus in quia odit aspernatur
      provident et ad vel distinctio recusandae totam quidem repudiandae
      omnis veritatis nostrum laboriosam architecto optio rem, dignissimos
      voluptatum beatae aperiam voluptatem atque. Beatae rerum dolores sunt`,
      isAdmin: true
    }
  ];