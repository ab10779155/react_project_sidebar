import React from "react";
import {Switch, Route} from "react-router-dom";
import LoginPageComponent  from "./login-page/constants/login-page-component.constant";
import MainPageComponent  from "./main-page/constants/main-page-component.constant";


const AppRoutes = () => {


    return (
        <React.Suspense fallback={<span>loading...</span>}>
        <Switch>
          <Route exact path="/" component={LoginPageComponent} />
          <Route path="/main" component={MainPageComponent} />
        </Switch>
      </React.Suspense>
    )
};

export default AppRoutes;
