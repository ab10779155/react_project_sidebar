import React, { SetStateAction, useState } from "react";
import { IconContext } from "react-icons";
import * as FaIcons from "react-icons/fa";
import { SidebarComponent } from "../sidebar/sidebar.component";
const HeaderComponent = ({ title, setHeader }: {title: string, setHeader: React.Dispatch<SetStateAction<string>>}) => {
    const [sidebar, setSidebar] = useState(false);

    const showSidebar = () => setSidebar(!sidebar);

    return (
        <div className="header-container">
            <IconContext.Provider value={{ color: "#fff" }}>
                <div className="navbar">
                    <div className="menu-bars">
                        <FaIcons.FaBars onClick={showSidebar} />
                    </div>
                </div>
                <SidebarComponent state={sidebar} setHeader={setHeader}/>
            </IconContext.Provider>
            <h1 className="header">{title}</h1>
        </div>
    );
};
export default HeaderComponent;
