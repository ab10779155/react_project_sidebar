import React from "react";

export const LoaderComponent = () => {

    return (
        <div className="loader-container">
          <div className="loader"></div>
        </div>
      );

}