import React from "react";
import { TTabData } from "../../constants/types";
import { useLocalStorage } from "../hooks/use-local-storage";
import { TabComponent } from "./tab/tab-component";
import { TTabDataItem } from "./type";


export const TabsComponent = ({data}: TTabDataItem) => {
  
  
  
  const { checkAdmin } = useLocalStorage();

  return (
      <div className="block-tabs">
        
        {data.map((item: TTabData, index: number) => {

            if (!checkAdmin && item.isAdmin) {
              return false;
          } else {
            return  <TabComponent key={index} title={item.title} path={item.path} className={item.className} activeClassName={item.activeClassName} />
          }
        }
        )}
      </div>
  );
};

