import { TTabData } from "../../../constants/types";

export type TTabDataItem = {
  data: TTabData[];
}
