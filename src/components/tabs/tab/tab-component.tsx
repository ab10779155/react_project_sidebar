import React from "react";
import { NavLink } from "react-router-dom";
import { TTabComponentProps } from "./types";

export const TabComponent = ({title, path, className, activeClassName }:TTabComponentProps) => {
    return (
            <NavLink
                to={path}
                className={className}
                activeClassName={activeClassName}
            >
                {title}
            </NavLink>
        
        );
} 