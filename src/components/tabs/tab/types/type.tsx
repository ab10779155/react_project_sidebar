export type TTabComponentProps = {
  title: string;
  path: string; 
  className: string;
  activeClassName: string;
}

