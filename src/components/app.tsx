import React from "react";
import { useUserLoggedIn } from "./hooks/use-user-logged-in";

import AppRoutes  from "./app-routes";

export const App = () => {
  useUserLoggedIn();

  
  return (
    <div className="app">
      <AppRoutes />
    </div>
  );
};
