import React from "react";
import { TTabContentComponent } from "./types";



const ContentComponent = ({header, content}: TTabContentComponent) => {
    
    return (
        <div className="content-tabs">
            <div className="content  active-content">
                        <h2>{header}</h2>
                        <hr/>
                        <p>{content}</p>
            </div>
        </div>
    );
};

export default ContentComponent;


