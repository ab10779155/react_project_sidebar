import {TInitialFormState} from '../constants/type/t-initial-form-state';
export const initialFormState: TInitialFormState = {
    email: "",
    password: "",
};