import React, { lazy } from "react";
const LoginPageComponent = lazy(() => import("../login-page.component"));
export default LoginPageComponent;