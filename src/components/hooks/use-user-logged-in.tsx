import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { TUserLoggedIn } from "./types/user-logged-in";
import { useLocalStorage } from "./use-local-storage";

export const useUserLoggedIn = (): TUserLoggedIn => {
  const [loggedIn, setloggedIn] = useState<boolean>(false);

  const { getValue, state } = useLocalStorage();
  const history = useHistory();

  useEffect(() => {
    getValue("user");
  }, [getValue]);

  useEffect(() => {
    
    if (state) {
      setloggedIn(true);
      history.replace("/main/home/news");
    } else {
      setloggedIn(false);
      history.replace("/");
      
    }
  }, [history, state]);

  const location = useLocation();

useEffect(() => {
  return history.listen(() => {
    if (history.action === 'POP') {
      if (!loggedIn) {
        history.push("/");
      }
    }
  })
}, [history, location, loggedIn])

  return { loggedIn };
};
