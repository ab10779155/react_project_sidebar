import { SetStateAction } from "react";

export type TUserLoggedIn = {
    loggedIn: boolean;
    }
