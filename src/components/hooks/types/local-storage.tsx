import { TUser } from "./user";
export type TLocalStorage = {
  state: string | null | void;
  hasValue: (key: string) => void;
  getValue: (key: string) => void;
  setValue: (key: string, data: TUser) => void;
  removeValue: (key: string) => void;
  checkAdmin: boolean;
};
