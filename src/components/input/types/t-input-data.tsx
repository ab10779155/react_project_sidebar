
export type TInputData = {
    value: string; 
    onChange: React.ChangeEventHandler<HTMLInputElement>;
    onBlur: React.FocusEventHandler<HTMLInputElement>;
    name: string; 
    className: string;
    id: string; 
    type: string; 
    minLength?: number; 
    maxLength?: number; 
    placeholder: string; 
    required?: boolean;
}
