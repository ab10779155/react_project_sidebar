import {TInputData} from './types/t-input-data'

export const InputComponent = ({value, onChange, onBlur, name, className, id, type, minLength, maxLength, placeholder, required} :TInputData) => {
    return (
    <input
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        name={name}
        className={className}
        id={id}
        type={type}
        minLength={minLength}
        maxLength={maxLength}
        placeholder={placeholder}
        required={required}
    />
    );
};
