import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as GoIcons from "react-icons/go";
import * as IoIcons from "react-icons/io";
import { useLocalStorage } from "../hooks/use-local-storage";
import { NavLink, useHistory } from "react-router-dom";
import "../../styles/colors-scheme/colors-scheme.scss";
import { TSidebar } from "./types";


export const SidebarComponent = ({state, setHeader}: TSidebar) => {
  const hideSidebar = (): boolean => state;
  const history = useHistory();
  const { removeValue, checkAdmin } = useLocalStorage();
  

  const removeKeyFromLocale = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => {
    removeValue("user");
    history.replace("/");
  };

  const ChangeHeaderName =(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    setHeader(e.currentTarget.textContent as string);
  }

  return (
        <nav className={state ? "nav-menu active" : "nav-menu"}>
          <ul className="nav-menu-items" onClick={hideSidebar}>
            <li className="navbar-toggle"> 
            </li> 
            <li className="nav-text">
              <NavLink to="/main/home/news" onClick={ChangeHeaderName}>
                <AiIcons.AiFillHome />
                <span>Home</span>
              </NavLink>
            </li>
            <li className="nav-text">
              <NavLink to="/main/about" onClick={ChangeHeaderName}>
                <IoIcons.IoIosPaper />
                <span>About</span>
              </NavLink>
            </li>
            <li className="nav-text" >
              <NavLink to="/main/service" onClick={ChangeHeaderName}>
                <FaIcons.FaEnvelopeOpenText />
                <span>Service</span>
              </NavLink>
            </li>
            {checkAdmin ? (
              <li className="nav-text">
                <NavLink to="/main/admin" onClick={ChangeHeaderName}>
                  <IoIcons.IoMdPeople />
                  <span>Admin</span>
                </NavLink>
              </li>
            ) : null}

            <div className="btn-exit-container">
              <button
                className="btn-logout"
                type="button"
                onClick={removeKeyFromLocale}
              >
                <div className="icon-exit-container">
                  <GoIcons.GoSignOut />
                </div>
              </button>
            </div>
          </ul>
        </nav>
  );
};

