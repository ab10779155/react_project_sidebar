import { SetStateAction } from "react"

export type TSidebar = {
  state: boolean,
  setHeader: React.Dispatch<SetStateAction<string>>
}
