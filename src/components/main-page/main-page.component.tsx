import React, {  useState } from "react";
import { FooterComponent } from "../footer/footer.component";
import HeaderComponent from "../header/header.component";
import MainPageRoutes from "./routes/main-page-routes";

const MainPageComponent = () => {
  const [header, setHeader] = useState("Home");


  return (
    <>
      <HeaderComponent title={header} setHeader={setHeader}/>
      <div className="container-main">
        <MainPageRoutes />
        <FooterComponent />
      </div>  
    </>
  );
};

export default MainPageComponent;
