import React from "react";
import {Switch, Route} from "react-router-dom";
import  HomePage  from "../../../pages/home/constants/home-page.constants";
import  AboutPage  from "../../../pages/about/constants/about-page.constants";
import  ServicePage  from "../../../pages/service/constants/service-page.constants";
import  AdminPage  from "../../../pages/admin/constants/admin-page.constants";

const MainPageRoutes = () => {


    return (
        <React.Suspense fallback={<span className="pre-loading">loading, please wait...</span>}>
            <Switch>
                <Route path="/main/home" component={HomePage}/>
                <Route path="/main/about" component={AboutPage} />
                <Route path="/main/service" component={ServicePage} />
                <Route path="/main/admin" component={AdminPage} />
            </Switch>
        </React.Suspense>
    )
};

export default MainPageRoutes;
