import React, {lazy} from "react";
const MainPageComponent = lazy(() => import("../main-page.component"));
export default MainPageComponent;