import * as Yup from "yup";


export const BasicFormSchema = Yup.object().shape({
    email: Yup.string()
        .email("некорректный e-mail")
        .required("поле e-mail не может быть пустым"),
    password: Yup.string()
        .required("поле password не может быть пустым")
        .matches(/(?=^.{12,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, 
        'некорректный пароль. Пароль должен содержать минимум 12 символов (4 цифры, только английские буквы, 1 заглавную букву, 2 спецсимвола')
});

