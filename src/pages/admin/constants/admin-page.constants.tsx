import React, { lazy } from "react";
export const AdminPage = lazy(() => import("../Admin"));
export default AdminPage;