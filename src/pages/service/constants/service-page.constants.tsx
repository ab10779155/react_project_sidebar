import React, { lazy } from "react";
export const ServicePage = lazy(() => import("../Service"));
export default ServicePage;