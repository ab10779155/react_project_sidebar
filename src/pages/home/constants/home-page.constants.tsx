import React, { lazy } from "react";
const HomePage = lazy(() => import("../home-page.component"));
export default HomePage;