import React  from 'react';
import {TabsComponent} from '../../components/tabs/tabs-component';
import { TabData } from '../../constants/tab-data.constant';
import HomeRoutes from './home-routes';



const HomePageComponent = () => {

  return (
      <div className="container">
        <TabsComponent data={TabData}  />
        <HomeRoutes />
      </div>
  );
}



export default HomePageComponent;

