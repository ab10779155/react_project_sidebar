import React from "react";
import {Switch, Route} from "react-router-dom";
import NewsPageComponent from "./news/news-page.component";
import PersonalPageComponent from "./personal/personal-page.component";
import SciencePageComponent from "./science/science-page.component";
import WorldPageComponent from "./world/world-page.component";


const HomeRoutes = () => {


    return (
        <React.Suspense fallback={<span>loading...</span>}>
            <Switch>
                <Route path="/main/home/news" component={NewsPageComponent} />
                <Route path="/main/home/science" component={SciencePageComponent} />
                <Route path="/main/home/world" component={WorldPageComponent} />
                <Route path="/main/home/personal" component={PersonalPageComponent} />
            </Switch>
        </React.Suspense>
    )
};

export default HomeRoutes;
