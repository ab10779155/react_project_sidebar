import React  from 'react';
import ContentComponent from '../../../components/content/content.component';



const PersonalPageComponent = () => {

    return (
        <ContentComponent 
            header="Personal"  
            content="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos sed
            nostrum rerum laudantium totam unde adipisci incidunt modi alias!
            Accusamus in quia odit aspernatur provident et ad vel distinctio
            recusandae totam quidem repudiandae omnis veritatis nostrum laboriosam
            architecto optio rem, dignissimos voluptatum beatae aperiam voluptatem
            atque. Beatae rerum dolores sunt. Lorem ipsum dolor sit amet,
            consectetur adipisicing elit. Eos sed nostrum rerum laudantium totam
            unde adipisci incidunt modi alias! Accusamus in quia odit aspernatur
            provident et ad vel distinctio recusandae totam quidem repudiandae
            omnis veritatis nostrum laboriosam architecto optio rem, dignissimos
            voluptatum beatae aperiam voluptatem atque. Beatae rerum dolores sunt"/>
    );
}



export default PersonalPageComponent;
